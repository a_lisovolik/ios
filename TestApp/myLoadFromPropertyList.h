//
//  myLoadFromPropertyList.h
//  TestApp
//
//  Created by alexander_lisovolik on 21.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface myLoadFromPropertyList : NSObject
+ (NSArray*) allUsers;


@end
