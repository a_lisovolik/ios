//
//  myWebService+Purchases.m
//  TestApp
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myWebService+Purchases.h"
#import "Models.h"

static NSString * addPurchaseString = @"AddPurchase";
static NSString * getPurchaseString = @"GetPurchase";
static NSString * getPurchaseByLocationFullString = @"GetPurchaseByLocationFull";
static NSString * getBusinessByLocationString = @"GetBusinessByLocation";

@implementation myWebService (Purchases)

- (void)addPurchase:(id<JSONRepresentation>)purchase success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure
{
    NSString * querry = [NSString stringWithFormat:DMBaseQuerryString, addPurchaseString, purchase.JSONRepresentation];
    //NSLog(@"query for add purchase %@", querry);
    [[myWebService sharedInstance] sendRequest:querry success:success failure:failure];
}

- (void)getPurchase:(id<JSONRepresentation>)purchase success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure
{
    NSString * querry = [NSString stringWithFormat:DMBaseQuerryString, getPurchaseString, purchase.JSONRepresentation];
    [[myWebService sharedInstance] sendRequest:querry success:success failure:failure];
}

- (void)getPurchaseByLocationFull:(id<JSONRepresentation>)purchase success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure
{
    NSString * querry = [NSString stringWithFormat:DMBaseQuerryString, getPurchaseByLocationFullString, purchase.JSONRepresentation];
    [[myWebService sharedInstance] sendRequest:querry success:success failure:failure];    
}

- (void)getBusinessByLocation:(id<JSONRepresentation>)business success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure
{
    NSString * querry = [NSString stringWithFormat:DMBaseQuerryString, getBusinessByLocationString, business.JSONRepresentation];
    //NSLog(@"query for GetBusinnesByLocation  %@", querry);
    [[myWebService sharedInstance] sendRequest:querry success:success failure:failure];
}

@end
