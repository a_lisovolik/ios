//
//  Models.h
//  TestApp
//
//  Created by alexander_lisovolik on 28.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JSONRepresentation <NSObject>
-(NSString*) JSONRepresentation;
@end
