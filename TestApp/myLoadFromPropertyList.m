//
//  myLoadFromPropertyList.m
//  TestApp
//
//  Created by alexander_lisovolik on 21.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myLoadFromPropertyList.h"

@implementation myLoadFromPropertyList

+ (NSArray*) allUsers
{
    NSArray* myDict = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"myAppPropList" ofType:@"plist"]];
    //NSArray* myUserArray = [[NSArray alloc] initWithObjects: myDict, nil];
    return myDict;
}
@end
