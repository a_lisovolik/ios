//
//  myWebService+Auth.h
//  TestApp
//
//  Created by alexander_lisovolik on 28.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myWebService.h"



@interface myWebService (Auth)

- (void)loginUser:(id<JSONRepresentation>)user success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure;
- (void)logout:(id<JSONRepresentation>)user success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure;
- (void)registerUser:(id<JSONRepresentation>)user success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure;

@end