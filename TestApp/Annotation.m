//
//  Annotation.m
//  TestApp
//
//  Created by alexander_lisovolik on 11.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation
@synthesize coordinate;
@synthesize title;
@synthesize subtitle;


- (void)dealloc {
    self.title = nil;
    self.subtitle = nil;
}

@end