//
//  myUser.h
//  TestApp
//
//  Created by alexander_lisovolik on 21.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Models.h"

@interface myUser: NSObject 
@property (strong, nonatomic) NSString* login;
@property (strong, nonatomic) NSString* password;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* sessionID;
@property (strong, nonatomic) NSString* zipCode;

+(myUser*) selectCurrentUser: (NSString*) alogin : (NSString*) apassword;
+(myUser*) getUserFromPropList: (NSDictionary*) userDictionary;
-(NSString*) JSONRepresentation;
@end

@interface LoginUserModel:NSObject<JSONRepresentation>
@property (strong, nonatomic)NSString* login;
@property (strong, nonatomic)NSString* password;
//+ (LoginUserModel*)instance;
@end

@interface LogoutModel:NSObject<JSONRepresentation>
@property (strong, nonatomic)NSString* sessionID;
//+ (LoginUserModel*)instance;
@end

@interface RegistrationModel:NSObject<JSONRepresentation>
@property (strong, nonatomic)NSString* name;
@property (strong, nonatomic)NSString* email;
@property (strong, nonatomic)NSString* password;
@property (strong, nonatomic)NSString* zipcode;
//+ (LoginUserModel*)instance;
@end

@interface GetPurchaseModel:NSObject<JSONRepresentation>
@property (strong, nonatomic)NSString* idPurchase;
@property (strong, nonatomic)NSString* latPurchase;
@property (strong, nonatomic)NSString* lonPurchase;
@end

@interface AddPurchaseModel:NSObject<JSONRepresentation>
@property (strong, nonatomic)NSString* sessionID;
@property (strong, nonatomic)NSString* idFactual;
@property (strong, nonatomic)NSString* description;
@property (strong, nonatomic)NSString* info;
@property (strong, nonatomic)NSString* idCategory;
@property (strong, nonatomic)NSString* dateEnd;
@property (strong, nonatomic)NSString* dateStart;

@end


@interface GetPurchaseByLocationFullModel:NSObject<JSONRepresentation>
@property (strong, nonatomic)NSString* RadiusPurchase;
@property (strong, nonatomic)NSString* latPurchase;
@property (strong, nonatomic)NSString* lonPurchase;
@end

@interface GetBusinessByLocationModel:NSObject<JSONRepresentation>
@property (strong, nonatomic)NSString* currentBusinessPage;
@property (strong, nonatomic)NSString* lat;
@property (strong, nonatomic)NSString* lon;
@property (strong, nonatomic)NSString* radius;
@end













