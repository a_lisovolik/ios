//
//  myWebService.h
//  TestApp
//
//  Created by alexander_lisovolik on 28.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JSONRepresentation;

static NSString * DMBaseQuerryString = @"%@&data=%@";

@interface myWebService : NSObject
+ (myWebService*)sharedInstance;
- (void) sendRequest:(NSString*) query success:(void (^)(id JSON))success failure:(void (^)(NSError *error, id JSON))failure;



@end
