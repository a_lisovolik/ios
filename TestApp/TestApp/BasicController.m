//
//  BasicController.m
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "BasicController.h"

@interface BasicController ()
@property (weak, nonatomic) UIView* activeView;

@end

@implementation BasicController 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:)  name: UIKeyboardDidShowNotification  object:nil];
    [self setupScrollView];
    
}

-(void) setBackgroundImageForView: (UIView*) someView
{
      UIGraphicsBeginImageContext(someView.frame.size);
     [[UIImage imageNamed:@"backgrnd3.jpeg"] drawInRect:someView.bounds];
     UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();   
     someView.backgroundColor = [UIColor colorWithPatternImage:image];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupScrollView
{
    if (!self.scrollView)
    {
        self.scrollView = [[UIScrollView alloc] initWithFrame: self.view.bounds];
    }
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.contentSize = (CGSize){320,500};
    self.scrollView.bounces = YES;
    self.scrollView.scrollEnabled = YES;
    
    for (UIView *view in self.view.subviews){
        [self.view removeFromSuperview];
        [self.scrollView addSubview:view];
    }

    self.view = self.scrollView;
}

- (CGSize)contentSizeForScroll
{
    CGFloat height = 0;
    for (UIView* view in self.scrollView.subviews){
        CGFloat temp = view.frame.origin.y + view.frame.size.height;
        if (height < temp ) height = temp;
    }
    
    return CGSizeMake(self.view.frame.size.width, height);
}



- (void) textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeView = textField;
    return YES;
}

- (void)scrollViewToInputView:(UIView*)inputView viewPort:(CGRect)viewPort
{
    CGRect targetRect = CGRectInset(inputView.frame, 0, -20.0);
    if (!CGRectContainsRect(viewPort, targetRect))
        {
            [self.scrollView scrollRectToVisible:targetRect animated:YES];
        }
}


- (void) keyboardWillHide :(NSNotification* ) notification
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    self.scrollView.scrollEnabled = NO;

}

- (void) keyboardWillShow:(NSNotification*) notification
{
   
    self.scrollView.scrollEnabled = YES;
    
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    CGFloat height = self.view.frame.size.height - kbSize.height;
    if (height < kbSize.height)
    {
        CGPoint offsetPoint = {0, kbSize.height - height};
        [self.scrollView setContentOffset:offsetPoint animated:YES];
    }

}


- (void) keyboardDidShow :(NSNotification*) notification
{
    
}



@end
