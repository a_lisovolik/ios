//
//  ViewController.m
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Annotation.h"
#import "myDetailPurchase.h"
#import "myPurchaseModel.h"
#import "PurchaseSingleton.h"
#import "myBusiness.h"
#import "myBusinessSingleton.h"



@interface ViewController()<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *map;
- (IBAction)btnSetRegion:(id)sender;
- (IBAction)userLocation:(id)sender;

@end

@implementation ViewController
@synthesize lat;
@synthesize lon;
@synthesize purchaseList;



- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKPinAnnotationView *annotationView = nil;
    
   // if (annotation != mapView.userLocation)
    {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annotationView.rightCalloutAccessoryView = infoButton;
        annotationView.animatesDrop = YES;
        annotationView.canShowCallout = YES;
        [annotationView setPinColor:MKPinAnnotationColorRed];
    }
    
    return annotationView;
}


-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    myDetailPurchase* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"myDetailPurchase"];
    myPurchaseModel* purchase = [myPurchaseModel getPurchaseByAnnotation:view.annotation];
    if (purchase.pathToFile.length > 0)
    {
        vc.imageName = purchase.pathToFile;
    }
    [self.navigationController pushViewController:vc animated:YES];    
    
    vc.goodRating.text = purchase.goodRating;
    vc.badRating.text  = purchase.badRating;
    vc.descriptionPurchase.text = purchase.discriptionPurchase;
    vc.business.text = purchase.businessName;
    
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.map.delegate = self;
    self.purchaseList = [[NSMutableArray alloc] init];
    //Set Region
    [self btnSetRegion:nil];
    [self getPurchaseByLocation];
    //[self userLocation:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload {
    [self setMap:nil];
    [super viewDidUnload];
}


- (IBAction)btnSetRegion:(id)sender
{
    CLLocationCoordinate2D startCoord;
    if ((self.lat != nil) && (self.lon != nil))
    {
        CLLocation *loc = [[CLLocation alloc] initWithLatitude: [self.lat doubleValue] longitude:[self.lon doubleValue]];
        startCoord = loc.coordinate;
    }
    else
    {
       startCoord.latitude = 37.7749295;
       startCoord.longitude = -122.4194155;
    }

    
    self.map.mapType = MKMapTypeStandard;    
    [self.map setRegion:MKCoordinateRegionMakeWithDistance(startCoord, 5000, 5000) animated:YES];


}

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber
{
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}

-(NSString*)generateRandomString
{
    NSMutableString* string = [NSMutableString stringWithCapacity:49];
    for (int i = 0; i < 49; i++) {
        [string appendFormat:@"%C", (unichar)('a' + arc4random_uniform(25))];
    }
    return string;
}


-(void) getPurchaseByLocation
{
    //Функция не возвращает список
/*    GetPurchaseByLocationFullModel* model = [GetPurchaseByLocationFullModel new];
   
    model.latPurchase = @"37.7749295";
    model.lonPurchase = @"-122.4194155";
    model.RadiusPurchase = @"50";
    //Progress
    [[myProgress sharedInstance] showIndicator:self];
    
    [[myWebService sharedInstance] getPurchaseByLocationFull:model success:^(id JSON) {
        
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
        
        lat = [NSNumber numberWithDouble: 37.7749295];
        lon = [NSNumber numberWithDouble: -122.4194155];
        NSLog(@" ----- LIST PURCHASE ----  %@", JSON);
        [self loadPurchaseListFromJSON:JSON];
        
        //  NSLog(@"%@", [JSON description]);
    } failure:^(NSError *error, id JSON) {
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                     message:[NSString stringWithFormat:@"%@",error]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // NSLog(@"%@", error);
        [av show];
        
    }];*/
    
    //тк не работает getPurchaseByLocationFull, то просто циклом получаю и отображаю на карте
    
    int i;
    for (i=24; i<35; i++)
    {
    GetPurchaseModel* model = [GetPurchaseModel new];    
    model.idPurchase = [NSString stringWithFormat:@"%d", i];
    //model.idPurchase = @"36";
    model.latPurchase = @"37.7749295";
    model.lonPurchase = @"-122.4194155";
    //Progress
    [[myProgress sharedInstance] showIndicator:self];
    [[myWebService sharedInstance]getPurchase:model success:^(id JSON) {
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
        [self.purchaseList addObject:JSON];

    } failure:^(NSError *error, id JSON) {
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                     message:[NSString stringWithFormat:@"%@",error]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        
    }];
        
    }

}


- (IBAction)userLocation:(id)sender
{
    //[self.map setShowsUserLocation:YES];
    NSMutableArray* tmpAnnotationArray = [[NSMutableArray alloc] init];
    //map center
    CLLocationCoordinate2D startCoord;
    if ((self.lat != nil) && (self.lon != nil))
    {
        CLLocation *loc = [[CLLocation alloc] initWithLatitude: [self.lat doubleValue] longitude:[self.lon doubleValue]];
        startCoord = loc.coordinate;
    }
    else
    {
        startCoord.latitude = 37.7749295;
        startCoord.longitude = -122.4194155;
    }
    //Загружаю список PurchaseByLocation
    
    int i;
    NSUInteger keyCount = [purchaseList count];
    for (i=0; i<keyCount; i++)
    {
        NSDictionary* dict = [[NSDictionary alloc] init];
        myPurchaseModel* purchase = [myPurchaseModel getPurchase:dict];
        Annotation *annotation = [Annotation new];
        purchase.address = [[[[purchaseList objectAtIndex:i] objectForKey:@"data"] objectForKey:@"business"] objectForKey:@"address"];
        purchase.businessName = [[[[purchaseList objectAtIndex:i] objectForKey:@"data"] objectForKey:@"business"] objectForKey:@"name"];
        purchase.discriptionPurchase = [[[purchaseList objectAtIndex:i] objectForKey:@"data"] objectForKey:@"description"];
        purchase.goodRating = [NSString stringWithFormat:@"Good - %d",(rand() % 101)];
        purchase.badRating = [NSString stringWithFormat:@"Bad - %d",(int)(rand() % 101)];
        purchase.discriptionPurchase = [[[purchaseList objectAtIndex:i] objectForKey:@"data"] objectForKey:@"description"];
        NSMutableArray* tmp = [[NSMutableArray alloc] init];
        tmp = [[[purchaseList objectAtIndex:i] objectForKey:@"data"] objectForKey:@"photos"];
        if (tmp.count > 0)
            {                
                purchase.pathToFile = [[tmp objectAtIndex:0] objectForKey:@"pathToFile"];
            }
        annotation.purchase = purchase;
        annotation.title = purchase.address;
        annotation.subtitle = purchase.goodRating;
        
        annotation.coordinate = CLLocationCoordinate2DMake([self randomFloatBetween:37.685 and:37.79],[self randomFloatBetween:-122.4018 and:-122.490]);
        [tmpAnnotationArray addObject: annotation];
    }
    
    [self.map addAnnotations:tmpAnnotationArray];
    
}

- (void)getBusinessByLocation
{
    GetBusinessByLocationModel* model = [GetBusinessByLocationModel new];
    model.currentBusinessPage = @"1";
    model.lat= @"37.7749295";
    model.lon = @"-122.4194155";
    model.radius = @"50";
    
    //Progress
    [[myProgress sharedInstance] showIndicator:self];
    
    
    [[myWebService sharedInstance] getBusinessByLocation:model
                                                 success:^(id JSON)
     {         
         [[myProgress sharedInstance] hideIndicator:self];
         //Load selected business.
         NSArray* busArray = [myBusinessList loadBusinessListFromJSON: JSON];
         myBusinessList* bl = [self.storyboard instantiateViewControllerWithIdentifier:@"myBusinessList"];
         self.modalTransitionStyle = UIModalTransitionStylePartialCurl;
         
         if (busArray.count)
         {
             bl.businessList = busArray;
             [self.navigationController presentViewController:bl animated:YES completion:nil];
            
         }
         
     }
                                                 failure:^(NSError *error, id JSON)
     {
         [[myProgress sharedInstance] hideIndicator:self];
         UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                      message:[NSString stringWithFormat:@"%@",error]
                                                     delegate:nil
                                            cancelButtonTitle:@"OK" otherButtonTitles:nil];
         NSLog(@"%@", error);
         [av show];
         
     }];
}


//Add new Purchase
- (IBAction)newPurchasePress:(id)sender
{
    //Сразу отображаю список бизнесов, чтобы взять idFactual
    [self getBusinessByLocation];
    
    if ([myBusinessSingleton sharedInstance].business.idFactual != nil)
    {
        myAddPurchaseController* apc = [self.storyboard instantiateViewControllerWithIdentifier:@"myAddPurchaseController"];
        self.modalTransitionStyle = UIModalTransitionStylePartialCurl;
        [self.navigationController presentViewController:apc animated:YES completion:nil];
    }
    
    
}

- (IBAction)GetPurchase:(id)sender
{
    [self getPurchaseByLocation]; //фактически получаю по ID
    [self userLocation:nil];
    
}

- (IBAction)closeBtnPress:(id)sender
{
    exit(0);
}

@end
