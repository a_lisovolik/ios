//
//  myForgotController.m
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myForgotController.h"

@interface myForgotController ()

@end

@implementation myForgotController
- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //set background
    [super setBackgroundImageForView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
