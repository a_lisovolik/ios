//
//  myBusinessSingleton.m
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myBusinessSingleton.h"

@implementation myBusinessSingleton
@synthesize business;

+ (myBusinessSingleton*)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static myBusinessSingleton* _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
}
@end
