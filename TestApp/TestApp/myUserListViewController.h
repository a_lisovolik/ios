//
//  myUserListViewController.h
//  TestApp
//
//  Created by alexander_lisovolik on 21.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicController.h"
@class myUser;

@protocol UserPickerProtocol
@required
-(void) didPickedUser: (myUser*) user contoller:(UIViewController*) controller;
-(void) didPickUnAuthorizedUser;

@end

@interface myUserListViewController : BasicController;
@property (strong, nonatomic) NSArray* userList;
@property (strong, nonatomic)id<UserPickerProtocol> delegate;
- (IBAction)closeApplication:(id)sender;
- (IBAction)addNewUser:(id)sender;
- (IBAction)btnDeleteUserPress:(id)sender;

@end
