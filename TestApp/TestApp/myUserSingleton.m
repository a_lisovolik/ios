//
//  myUserSingleton.m
//  TestApp
//
//  Created by alexander_lisovolik on 21.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myUserSingleton.h"

@implementation myUserSingleton
@synthesize user;
+ (myUserSingleton*)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static myUserSingleton* _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
}


@end
