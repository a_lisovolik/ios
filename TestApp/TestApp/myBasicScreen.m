//
//  myBasicScreen.m
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myBasicScreen.h"
#import "myUserSingleton.h"
#import "myUser.h"
#import "myWebService.h"
#import "myWebService+Purchases.h"
#import "myProgress.h"
#import "ViewController.h"
#import "myBusinessList.h"
#import "myBusiness.h"
#import "myBusinessSingleton.h"


@interface myBasicScreen ()<UserPickerBusiness>
@property (strong, nonatomic) NSString * myUserName;
@property (weak, nonatomic) IBOutlet UILabel *nameLabelText;
@end

@implementation myBasicScreen

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   self.nameLabelText.text = [myUserSingleton sharedInstance].user.login;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)getPurchasePress:(id)sender
{
    GetPurchaseModel* model = [GetPurchaseModel new];
    model.idPurchase = @"27";
    model.latPurchase = @"47";
    model.lonPurchase = @"-80";
    //Progress
    [[myProgress sharedInstance] showIndicator:self];
    [[myWebService sharedInstance]getPurchase:model success:^(id JSON) {
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
    } failure:^(NSError *error, id JSON) {
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                     message:[NSString stringWithFormat:@"%@",error]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        
    }];

}


- (IBAction)getPurchaseByLocation:(id)sender
{
    GetPurchaseByLocationFullModel* model = [GetPurchaseByLocationFullModel new];
    myBusinessSingleton* tmpBusiness = [myBusinessSingleton sharedInstance];
    /*
    model.latPurchase = @"25.788";
    model.lonPurchase = @"-80.22";*/
    model.RadiusPurchase = @"50";
    //Progress
    [[myProgress sharedInstance] showIndicator:self];
    
    [[myWebService sharedInstance] getPurchaseByLocationFull:model success:^(id JSON) {
    
    //Progress
    [[myProgress sharedInstance] hideIndicator:self];
        
    ViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        vc.lat = [NSNumber numberWithDouble: 25.788];
        vc.lon = [NSNumber numberWithDouble: -80.22];
        [vc loadPurchaseListFromJSON:JSON];
    [self.navigationController pushViewController:vc animated:YES];
        
      //  NSLog(@"%@", [JSON description]);
    } failure:^(NSError *error, id JSON) {
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                     message:[NSString stringWithFormat:@"%@",error]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
       // NSLog(@"%@", error);
        [av show];
        
    }];
    
}



- (IBAction)getBusinessByLocation:(id)sender
{
    GetBusinessByLocationModel* model = [GetBusinessByLocationModel new];
    model.currentBusinessPage = @"1";
    model.lat= @"25.788";
    model.lon = @"-80.22";
    model.radius = @"50";
    
    //Progress
    [[myProgress sharedInstance] showIndicator:self];
    
    
    [[myWebService sharedInstance] getBusinessByLocation:model
        success:^(id JSON)
     {       
        
       // NSLog(@"%@", [JSON description]);
        [[myProgress sharedInstance] hideIndicator:self];
        //Load selected business.
        NSArray* busArray = [myBusinessList loadBusinessListFromJSON: JSON];
        myBusinessList* bl = [self.storyboard instantiateViewControllerWithIdentifier:@"myBusinessList"];
        self.modalTransitionStyle = UIModalTransitionStylePartialCurl;
        
        if (busArray.count)
            {
                bl.delegate = self;
                bl.businessList = busArray;
                [self.navigationController presentViewController:bl animated:YES completion:nil];
                
            }
        
     }
        failure:^(NSError *error, id JSON)
            {
                [[myProgress sharedInstance] hideIndicator:self];
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                     message:[NSString stringWithFormat:@"%@",error]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
                NSLog(@"%@", error);
                [av show];
        
            }];
}



-(void) didPickedBusiness:(myBusiness *)business contoller:(UIViewController *)controller
{
    
    //myBusinessSingleton* businessInfoSingleton = [myBusinessSingleton sharedInstance];
    [self dismissModalViewControllerAnimated:NO];
    
}

@end










