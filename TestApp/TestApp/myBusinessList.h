//
//  myBusinessList.h
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "BasicController.h"
#import "myBusiness.h"


@protocol UserPickerBusiness
@required
-(void) didPickedBusiness: (myBusiness*) user contoller:(UIViewController*) controller;

@end

@interface myBusinessList : BasicController;
@property (strong, nonatomic) NSArray* businessList;
@property (strong, nonatomic)id<UserPickerBusiness> delegate;
+(NSArray*)loadBusinessListFromJSON: (NSDictionary*) JSON;
- (IBAction)pressCancelButton:(id)sender;
@end
