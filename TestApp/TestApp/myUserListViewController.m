//
//  myUserListViewController.m
//  TestApp
//
//  Created by alexander_lisovolik on 21.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myUserListViewController.h"
#import "myUser.h"
#import "myUserSingleton.h"

@interface myUserListViewController ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation myUserListViewController
@synthesize userList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    //set background
    [super setBackgroundImageForView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.userList.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    cell.textLabel.text = [[self.userList objectAtIndex:indexPath.row] objectForKey:@"login"];
    return cell;
}

/*срабатывает когда пользователь выбрал определенную ячейку*/
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    myUser* pikcedUser = [myUser getUserFromPropList: self.userList[indexPath.row]];
    myUserSingleton* userInfoSingleton = [myUserSingleton sharedInstance];
    userInfoSingleton.user = pikcedUser;
    [self.delegate didPickedUser:pikcedUser contoller:self];
}


- (IBAction)closeApplication:(id)sender
{
    exit(0);
}

- (IBAction)addNewUser:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

//Delete user frol plist
- (IBAction)btnDeleteUserPress:(id)sender
{
    
}
@end
