//
//  myDetailPurchase.m
//  TestApp
//
//  Created by alexander_lisovolik on 11.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myDetailPurchase.h"

@interface myDetailPurchase ()

@end

@implementation myDetailPurchase
@synthesize imageName;


void UIImageFromURL( NSURL * URL, void (^imageBlock)(UIImage * image), void (^errorBlock)(void) )
{
    dispatch_async( dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0 ), ^(void)
                   {
                       NSData * data = [[NSData alloc] initWithContentsOfURL:URL];
                       UIImage * image = [[UIImage alloc] initWithData:data];
                       dispatch_async( dispatch_get_main_queue(), ^(void){
                           if( image != nil )
                           {
                               imageBlock(image);
                           } else {
                               errorBlock();
                           }
                       });
                   });
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //set background
    [super setBackgroundImageForView:self.view];
    
    UIImageFromURL( [NSURL URLWithString:[NSString stringWithFormat:@"http://pgood.ru/userfiles/image/gallery/img0%d.jpg", (arc4random() % 9)]], ^( UIImage * image )
    {
       [self.imagePurchase setImage:image];
    }, ^(void){
        NSLog(@"%@",@"error!");
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setGoodRating:nil];
    [self setBadRating:nil];
    [self setImagePurchase:nil];
    [self setBusiness:nil];
    [self setDescriptionPurchase:nil];
    [self setDescriptionPurchase:nil];
    [super viewDidUnload];
}
@end
