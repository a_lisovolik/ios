//
//  myUserSingleton.h
//  TestApp
//
//  Created by alexander_lisovolik on 21.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "myUser.h"

@interface myUserSingleton : NSObject
@property (strong, nonatomic) myUser* user;
+ (myUserSingleton*)sharedInstance;


@end
