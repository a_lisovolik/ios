//
//  myLoginController.h
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicController.h"
#import "myUser.h"




@interface myLoginController : BasicController
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;
@property (weak, nonatomic) IBOutlet UIButton *btnRemember;
@property (strong, nonatomic) NSMutableData *receivedData;
- (IBAction)btnRememberPress:(UIButton *)sender;
- (IBAction)btnEnterPress:(UIButton *)sender;
- (IBAction)testPress:(id)sender;

@end
