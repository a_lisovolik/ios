//
//  myDetailPurchase.h
//  TestApp
//
//  Created by alexander_lisovolik on 11.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "BasicController.h"

@interface myDetailPurchase : BasicController
@property (weak, nonatomic) IBOutlet UILabel *goodRating;
@property (weak, nonatomic) IBOutlet UILabel *badRating;
@property (weak, nonatomic) IBOutlet UIImageView *imagePurchase;
@property (weak, nonatomic) IBOutlet UILabel *business;
@property (weak, nonatomic) IBOutlet UILabel *descriptionPurchase;

@property (weak, nonatomic) NSString* imageName;

@end
