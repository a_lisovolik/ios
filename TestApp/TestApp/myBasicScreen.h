//
//  myBasicScreen.h
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicController.h"
#import "myBusinessList.h"
#import "myBusiness.h"
#import "myBusinessSingleton.h"

@interface myBasicScreen : BasicController

- (IBAction)getPurchasePress:(id)sender;
- (IBAction)getPurchaseByLocation:(id)sender;
- (IBAction)getBusinessByLocation:(id)sender;

@end
