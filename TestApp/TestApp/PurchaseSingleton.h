//
//  PurchaseSingleton.h
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "myPurchaseModel.h"

@interface PurchaseSingleton : NSObject
@property (strong, nonatomic) myPurchaseModel* purchase;
+ (PurchaseSingleton*)sharedInstance;
@end
