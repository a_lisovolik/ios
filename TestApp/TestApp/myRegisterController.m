//
//  myRegisterController.m
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myRegisterController.h"
#import "myUser.h"
#import "myWebService+Auth.h"
#import "myUserSingleton.h"
#import "myProgress.h"


@interface myRegisterController ()
@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *confPassword;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *zipcode;

@end

@implementation myRegisterController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //set background
    [super setBackgroundImageForView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnRegisterPress:(id)sender

{
    if ([self.password.text isEqualToString: self.confPassword.text])
    {
        //Progress
        [[myProgress sharedInstance] showIndicator:self];
        
        RegistrationModel* model = [RegistrationModel new];
        model.name = self.login.text;
        model.email = self.email.text;
        model.password = self.password.text;
        model.zipcode = self.zipcode.text;
        [[myWebService sharedInstance]registerUser:model success:^(id JSON)
        {
            //Progress
            [[myProgress sharedInstance] hideIndicator:self];
            
            myUserSingleton* userInfoSingleton = [myUserSingleton sharedInstance];
            userInfoSingleton.user.name =  self.login.text;
            userInfoSingleton.user.password = self.password.text;
            userInfoSingleton.user.email = self.email.text;
            userInfoSingleton.user.zipCode = self.zipcode.text;
            
            // Здесь получить sessionID
            userInfoSingleton.user.sessionID = [[JSON objectForKey:@"data"] objectForKey:@"sessionID"];
            NSLog(@"%@", JSON);
            if ([[JSON objectForKey:@"data"] objectForKey:@"sessionID"] != nil)
            {
                UIViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                [self.navigationController pushViewController:vc animated:YES];
                
            }

  
            
        } failure:^(NSError *error, id JSON) {
            //Progress
            [[myProgress sharedInstance] hideIndicator:self];
            
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                         message:[NSString stringWithFormat:@"%@",error]
                                                        delegate:nil
                                               cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [av show];
            
        }];
        
        
    }
    else
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error"
                                                     message:[NSString stringWithFormat:@"Paswword incorrect!"]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
    }


 
}


- (void)viewDidUnload
{
    [super viewDidUnload];
}
@end
