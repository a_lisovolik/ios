//
//  myBusinessList.m
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myBusinessList.h"
#import "myBusiness.h"
#import "myBusinessSingleton.h"
#import "myAddPurchaseController.h"

@interface myBusinessList ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation myBusinessList
@synthesize businessList;



+(NSArray*)loadBusinessListFromJSON: (NSDictionary*) JSON
{
    NSMutableArray* tmp = [[NSMutableArray alloc] init];
    NSDictionary* tmpDict = [[JSON objectForKey:@"data"] objectForKey:@"array"];
    NSUInteger keyCount = [tmpDict count];
    for (int i=0; i<keyCount; i++)
    {
        NSMutableDictionary* dict = [[[JSON objectForKey:@"data"] objectForKey:@"array"] objectAtIndex:i];
        //[tmp addObject:[dict objectForKey:@"name"]];
        [tmp addObject:[[[JSON objectForKey:@"data"] objectForKey:@"array"] objectAtIndex:i]];
    }
    
    return tmp;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    //set background
    [super setBackgroundImageForView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.businessList.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    cell.textLabel.text = [[self.businessList objectAtIndex:indexPath.row] objectForKey:@"name"];
    return cell;
}

/*срабатывает когда пользователь выбрал определенную ячейку*/
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    myBusiness* pickedBusiness = [myBusiness getBusinessList: self.businessList[indexPath.row]];
    myBusinessSingleton* businessInfoSingleton = [myBusinessSingleton sharedInstance];
    businessInfoSingleton.business = pickedBusiness;
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate didPickedBusiness:pickedBusiness contoller:self];
}

- (IBAction)pressCancelButton:(id)sender
{
   [self dismissViewControllerAnimated:YES completion:nil]; 
}

- (IBAction)addPurchasePress:(id)sender {
}
@end
