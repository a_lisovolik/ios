//
//  myBusinessSingleton.h
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "myBusiness.h"

@interface myBusinessSingleton : NSObject
@property (strong, nonatomic) myBusiness* business;
+ (myBusinessSingleton*)sharedInstance;
@end
