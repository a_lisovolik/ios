//
//  BasicController.h
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasicController : UIViewController <UITextFieldDelegate>
- (void) setupScrollView;
@property (strong, nonatomic) UIScrollView* scrollView;
-(void) setBackgroundImageForView: (UIView*) someView;
@end
