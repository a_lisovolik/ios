//
//  myLoginController.m
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myLoginController.h"
#import "myUserListViewController.h"
#import "myLoadFromPropertyList.h"
#import "myUser.h"
#import "myUserSingleton.h"
#import "myWebService+Auth.h"
#import "myProgress.h"
#import "ViewController.h"


@protocol Model
- (void)setModel:(myUser*)user;
@end

@interface myLoginController ()<UserPickerProtocol>
@property (weak, nonatomic) IBOutlet UITextField *edtLogin;
@property (weak, nonatomic) IBOutlet UITextField *edtPasssword;
@property (strong, nonatomic)myUser* user;
@end


@implementation myLoginController
@synthesize receivedData;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set background
    [super setBackgroundImageForView:self.view];
    
    //button "Remember me"
    [self.btnRemember setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
    [self.btnRemember setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateSelected];
    
    NSArray* userArray = [myLoadFromPropertyList allUsers];
    myUserSingleton* userInfoSingleton = [myUserSingleton sharedInstance];

    if ((userArray.count) && (userInfoSingleton.user.login.length == 0))
    {
        myUserListViewController* userListrContr = [[myUserListViewController alloc] initWithNibName:NSStringFromClass([myUserListViewController class]) bundle:[NSBundle mainBundle]];
        userListrContr.userList = userArray;
        userListrContr.delegate = self;
        self.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    //
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    userListrContr.view.frame = CGRectMake(0, 0, screenWidth - 200,screenHeight-200);
    
    //userListrContr.backgroundColor = [UIColor redColor];
 
    [self presentViewController:userListrContr animated:YES completion:nil];
    }
	// Do any additional setup after loading the view.
    if (self.user)
    {
        self.edtLogin.text = self.user.login;
        self.edtPasssword.text = self.user.password;
        self.edtPasssword.secureTextEntry =YES;
    }
    self.edtLogin.placeholder = @"enter your login";
    self.edtPasssword.placeholder =@"enter your password";

}

- (IBAction)btnRememberPress:(UIButton*)sender
{
    sender.selected = !sender.selected;
    
}


- (IBAction)testPress:(id)sender;
{
      LogoutModel* model = [LogoutModel new];
    model.sessionID = self.user.sessionID;
    [[myWebService sharedInstance]logout:model success:^(id JSON) {
        NSLog(@"%@", [JSON description]);
              } failure:^(NSError *error, id JSON) {
                  UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                               message:[NSString stringWithFormat:@"%@",error]
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
                  [av show];
                  
              }];
}


- (IBAction)btnEnterPress:(id)sender
{
    LoginUserModel* model = [LoginUserModel new];
    model.login = self.edtLogin.text;
    model.password = self.edtPasssword.text;
    
    //Progress
    [[myProgress sharedInstance] showIndicator:self];

    [[myWebService sharedInstance]loginUser:model
                                    success:^(id JSON)
            {
                myUserSingleton* userInfoSingleton = [myUserSingleton sharedInstance];
                userInfoSingleton.user.sessionID = [[JSON objectForKey:@"data"] objectForKey:@"sessionID"];
                //Progress
                [[myProgress sharedInstance] hideIndicator:self];
        
                if ([[JSON objectForKey:@"data"] objectForKey:@"sessionID"] != nil)
                    {
                        //Save User if button REMEMBER is selected
                        if (self.btnRemember.selected == YES)
                        {
                            NSString *myFile=[[NSBundle mainBundle] pathForResource:@"myAppPropList" ofType:@"plist"];
                            NSMutableArray* myDict = [NSMutableArray arrayWithContentsOfFile:myFile];
                            NSString *value;
                            BOOL flag;
                            flag = NO;
                            for (NSDictionary* tmpDict in myDict)
                            {
                                value = [tmpDict objectForKey:@"login"];
                                if ([value isEqualToString:model.login]) flag = YES;
                            }
                            if (!flag)
                            {
                                NSDictionary* newUser = [[NSDictionary alloc] initWithObjectsAndKeys:
                                             [NSNumber numberWithInteger:myDict.count + 1], @"id",
                                             model.login, @"login",
                                             model.password, @"password", nil];
                                [myDict addObject:newUser];
                                [myDict writeToFile:myFile atomically:YES];
                            }
                
                        }

                        //Show purchase map
                        ViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                        [self.navigationController pushViewController:vc animated:YES];
            
                    }

            }
                                    failure:^(NSError *error, id JSON)
    {
       [[myProgress sharedInstance] hideIndicator:self];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                                         message:[NSString stringWithFormat:@"%@",error]
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [av show];

    }];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UserPickerProtocol

-(void) didPickedUser: (myUser*) user contoller:(UIViewController*) controller;
{

    myUserSingleton* userInfoSingleton = [myUserSingleton sharedInstance];
    self.user = userInfoSingleton.user;
    if (user)
    {
        self.edtLogin.text = user.login;
        self.edtPasssword.text = user.password;
        self.edtPasssword.secureTextEntry =YES;
    }
    self.edtLogin.placeholder = @"enter your login";
    self.edtPasssword.placeholder =@"enter your password";
    [self dismissModalViewControllerAnimated:NO];
    
}



-(void) didPickUnAuthorizedUser
{
    
}


- (void)setModel:(myUser*)user
{
    self.user = user;
}


- (void)viewDidUnload {
    
    [self setBtnRemember:nil];
    [self setActIndicator:nil];
    [super viewDidUnload];
}

@end










