//
//  ViewController.h
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicController.h"
#import <MapKit/MapKit.h>
#import "myUser.h"
#import "myBusinessSingleton.h"
#import "myProgress.h"
#import "myWebService+Purchases.h"
#import "myWebService.h"
#import "myBusinessList.h"
#import "myUserSingleton.h"
#import "myAddPurchaseController.h"



@interface ViewController : BasicController <MKMapViewDelegate>
+(NSString*) generateRandomString;
- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber;

-(void) getPurchaseByLocation;
-(void)loadPurchaseListFromJSON: (NSDictionary*) JSON;
@property (strong, nonatomic) NSMutableArray* purchaseList;

- (void)getBusinessByLocation;


@property (nonatomic, readwrite) NSNumber *lat;
@property (nonatomic, readwrite) NSNumber *lon;
- (IBAction)newPurchasePress:(id)sender;
- (IBAction)GetPurchase:(id)sender;
- (IBAction)closeBtnPress:(id)sender;
@end;
