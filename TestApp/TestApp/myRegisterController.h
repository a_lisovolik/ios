//
//  myRegisterController.h
//  TestApp
//
//  Created by alexander_lisovolik on 14.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicController.h"

@interface myRegisterController : BasicController
- (IBAction)btnRegisterPress:(id)sender;

@end
