//
//  myWebService+Auth.m
//  TestApp
//
//  Created by alexander_lisovolik on 28.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myWebService+Auth.h"
#import "Models.h"



static NSString * DMLoginString = @"login";
static NSString * DMLogoutString = @"logout";
static NSString * DMRegistrationString = @"registration";


@implementation myWebService (Auth)

- (void)loginUser:(id<JSONRepresentation>)user success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure
{
    NSString * querry = [NSString stringWithFormat:DMBaseQuerryString,DMLoginString,user.JSONRepresentation];

    [[myWebService sharedInstance] sendRequest:querry success:success failure:failure];
}

- (void)logout:(id<JSONRepresentation>)user success:(void (^)(id))success failure:(void (^)(NSError *, id))failure
{
    NSString * querry = [NSString stringWithFormat:DMBaseQuerryString,DMLogoutString,user.JSONRepresentation];
    
    [[myWebService sharedInstance] sendRequest:querry success:success failure:failure];
}

-(void) registerUser:(id<JSONRepresentation>)user success:(void (^)(id))success failure:(void (^)(NSError *, id))failure
{
    NSString * querry = [NSString stringWithFormat:DMBaseQuerryString,DMRegistrationString,user.JSONRepresentation];
    
    [[myWebService sharedInstance] sendRequest:querry success:success failure:failure];
}
@end
