//
//  myUser.m
//  TestApp
//
//  Created by alexander_lisovolik on 21.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myUser.h"

@implementation myUser
@synthesize login;
@synthesize password;
@synthesize email;
@synthesize name;
@synthesize sessionID;
@synthesize zipCode;

+(myUser*) selectCurrentUser: (NSString*) alogin : (NSString*) apassword
{
    myUser* instance = [[myUser alloc] init];
    instance.login = alogin;
    instance.password = apassword;
    return instance;
       
}


+(myUser*) getUserFromPropList: (NSDictionary*) userDictionary
{
    myUser* instance = [[myUser alloc] init];
    
    instance.login = [userDictionary valueForKey:@"login"];
    instance.password = [userDictionary valueForKey:@"password"];
    return instance;
    
}

@end

@implementation GetBusinessByLocationModel

-(NSString*) JSONRepresentation
{
    return [NSString stringWithFormat:@"{\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\"}", @"currentBusinessPage", self.currentBusinessPage, @"lat",self.lat, @"lon",self.lon, @"radius", self.radius];
}
@end


@implementation GetPurchaseByLocationFullModel

-(NSString*) JSONRepresentation
{
    return [NSString stringWithFormat:@"{\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\"}", @"lat", self.latPurchase, @"lon",self.lonPurchase, @"radius",self.RadiusPurchase];
}
@end


@implementation AddPurchaseModel

-(NSString*) JSONRepresentation
{
    NSString* temDictionaryString = [NSString stringWithFormat:@"{idFactual:%@}",self.idFactual];
    return [NSString stringWithFormat:@"{\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\"}", @"sessionID", self.sessionID, @"user",temDictionaryString, @"description",self.description, @"info", self.info, @"idCategory", self.idCategory, @"dateEnd", self.dateEnd, @"dateStart", self.dateStart, @"idFactual", self.idFactual];
}
@end



@implementation GetPurchaseModel

-(NSString*) JSONRepresentation
{
    return [NSString stringWithFormat:@"{\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\"}", @"id", self.idPurchase, @"lat",self.latPurchase, @"lon",self.lonPurchase];
}
@end


@implementation LoginUserModel

-(NSString*) JSONRepresentation
{
    return [NSString stringWithFormat:@"{\"%@\":\"%@\",\"%@\":\"%@\"}", @"login", self.login, @"password",self.password];
}
@end

@implementation LogoutModel

-(NSString*) JSONRepresentation
{
    NSLog(@"%@", self.sessionID);
    return [NSString stringWithFormat:@"{\"%@\":\"%@\"}", @"sessionID", self.sessionID];
    
}
@end

@implementation RegistrationModel

-(NSString*) JSONRepresentation
{
    return [NSString stringWithFormat:@"{\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\",\"%@\":\"%@\"}", @"name", self.name, @"email",self.email, @"password", self.password, @"zipCode", self.zipcode];
}

@end