//
//  myAddPurchaseController.h
//  TestApp
//
//  Created by alexander_lisovolik on 25.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "BasicController.h"
#import "myUserSingleton.h"
#import "myPurchaseModel.h"
#import "myWebService.h"
#import "myWebService+Purchases.h"
#import "myBusinessSingleton.h"
#import "myProgress.h"

@interface myAddPurchaseController : BasicController
@property (weak, nonatomic) IBOutlet UITextField *titlePurchase;
@property (weak, nonatomic) IBOutlet UITextView *discriptionPurchase;
@property (weak, nonatomic) IBOutlet UIButton *dateStartPurchase;
@property (weak, nonatomic) IBOutlet UIButton *dateEndPurchase;
@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;
@property (weak,nonatomic) UIButton *pressedButton;
- (IBAction)backgrTouch:(id)sender;


- (IBAction)btnDSPress:(id)sender;
- (IBAction)btnDEPress:(id)sender;
- (IBAction)DateChange:(id)sender;

- (IBAction)addPurchasePress:(id)sender;
- (void)addPurchase;
@end
