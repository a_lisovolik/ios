//
//  myProgress.m
//  TestApp
//
//  Created by alexander_lisovolik on 11.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myProgress.h"


@implementation myProgress
+ (myProgress*)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static myProgress* _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] initWithFrame:[UIScreen mainScreen].bounds]; // or some other init method
    });
    _sharedObject.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    return _sharedObject;
}

-(void) showIndicator:(UIViewController*) controller
{
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    act.color = [UIColor whiteColor];
    act.center = self.center;
    [self addSubview:act];
    [act startAnimating];
    [controller.view addSubview:self];
}


-(void) hideIndicator:(UIViewController*) controller
{
    [self removeFromSuperview];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
