//
//  myPurchaseModel.h
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Annotation.h"

@interface myPurchaseModel : NSObject
@property (strong, nonatomic) NSString* address;
@property (strong, nonatomic) NSString* distance;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* phone;
@property (strong, nonatomic) NSString* goodRating;
@property (strong, nonatomic) NSString* badRating;
@property (strong, nonatomic) NSString* discriptionPurchase;
@property (strong, nonatomic) NSString* idFactual;
@property (strong, nonatomic) NSString* pathToFile;
@property (strong, nonatomic) NSString* businessName;
+(myPurchaseModel*) getPurchase: (NSDictionary*) purchaseDict;
+(myPurchaseModel*) getPurchaseByAnnotation: (Annotation*) purchaseAnnatation;
@end
