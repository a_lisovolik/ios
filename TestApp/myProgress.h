//
//  myProgress.h
//  TestApp
//
//  Created by alexander_lisovolik on 11.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface myProgress : UIView
+ (myProgress*)sharedInstance;
-(void) showIndicator:(UIViewController*) controller;
-(void) hideIndicator:(UIViewController*) controller;
@end
