//
//  Annotation.h
//  TestApp
//
//  Created by alexander_lisovolik on 11.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@class myPurchaseModel;

@interface Annotation : NSObject <MKAnnotation>
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic) myPurchaseModel *purchase;

@end
