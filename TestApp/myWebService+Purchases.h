//
//  myWebService+Purchases.h
//  TestApp
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myWebService.h"


@interface myWebService (Purchases)

- (void)addPurchase:(id<JSONRepresentation>)user success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure;
- (void)getPurchase:(id<JSONRepresentation>)user success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure;
- (void)getPurchaseByLocationFull:(id<JSONRepresentation>)user success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure;
- (void)getBusinessByLocation:(id<JSONRepresentation>)user success:(void (^)(id JSON))success failure:(void(^)(NSError* error, id JSON))failure;
@end
