//
//  myBusiness.m
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myBusiness.h"

@implementation myBusiness

+(myBusiness*) getBusinessList: (NSDictionary*) businessDictionary
{
    myBusiness* instance = [[myBusiness alloc] init];    
    instance.address = [businessDictionary valueForKey:@"address"];
    instance.city = [businessDictionary valueForKey:@"city"];
    instance.distance = [businessDictionary valueForKey:@"distance"];
    instance.idFactual = [businessDictionary valueForKey:@"idFactual"];
    instance.name = [businessDictionary valueForKey:@"name"];
    instance.phone= [businessDictionary valueForKey:@"phone"];
    instance.region = [businessDictionary valueForKey:@"region"];
    instance.zipCode = [businessDictionary valueForKey:@"zipcode"];
    return instance;
    
}

@end



