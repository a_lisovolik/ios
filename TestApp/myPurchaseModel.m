//
//  myPurchaseModel.m
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myPurchaseModel.h"

@implementation myPurchaseModel

+(myPurchaseModel*) getPurchase: (NSDictionary*) purchaseDict
{
   myPurchaseModel* instance = [[myPurchaseModel alloc] init];
   instance.address     = [purchaseDict valueForKey:@"address"];
   instance.distance    = [purchaseDict valueForKey:@"distance"];
   instance.name        = [purchaseDict valueForKey:@"name"];
   instance.phone       = [purchaseDict valueForKey:@"phone"];
   instance.badRating   = [purchaseDict valueForKey:@"badRating"];
   instance.goodRating  = [purchaseDict valueForKey:@"goodRating"];
   instance.idFactual   = [purchaseDict valueForKey:@"idFactual"];
   instance.pathToFile  = [purchaseDict valueForKey:@"pathToFile"];
   instance.businessName  = [purchaseDict valueForKey:@"businessName"];
   instance.discriptionPurchase = [purchaseDict valueForKey:@"discriptionPurchase"];

   return instance;
}

+(myPurchaseModel*) getPurchaseByAnnotation: (Annotation*) purchaseAnnatation
{
    myPurchaseModel* instance = [[myPurchaseModel alloc] init];
    instance.address     = purchaseAnnatation.purchase.address;
    instance.distance    = purchaseAnnatation.purchase.distance;
    instance.name        = purchaseAnnatation.purchase.name;
    instance.phone       = purchaseAnnatation.purchase.phone;
    instance.badRating   = purchaseAnnatation.purchase.badRating;
    instance.goodRating  = purchaseAnnatation.purchase.goodRating;
    instance.idFactual   = purchaseAnnatation.purchase.idFactual;
    instance.pathToFile  = purchaseAnnatation.purchase.pathToFile;
    instance.businessName = purchaseAnnatation.purchase.businessName;
    instance.discriptionPurchase = purchaseAnnatation.purchase.discriptionPurchase;
    
    return instance;
}
@end
