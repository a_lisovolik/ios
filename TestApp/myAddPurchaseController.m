//
//  myAddPurchaseController.m
//  TestApp
//
//  Created by alexander_lisovolik on 25.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myAddPurchaseController.h"

@interface myAddPurchaseController ()

@end

@implementation myAddPurchaseController
@synthesize datepicker;
@synthesize pressedButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	//set background
    [super setBackgroundImageForView:self.view];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addPurchase
{
    AddPurchaseModel* model = [AddPurchaseModel new];
    model.sessionID = [myUserSingleton sharedInstance].user.sessionID;
    model.description = self.discriptionPurchase.text;
    model.info = self.titlePurchase.text;
    model.idCategory = @"1";
    model.dateEnd = @"2013-02-01";
    model.dateStart = @"2013-01-12";
    model.idFactual = [myBusinessSingleton sharedInstance].business.idFactual;
    //Progress
    [[myProgress sharedInstance] showIndicator:self];
    [[myWebService sharedInstance]addPurchase:model success:^(id JSON) {
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
        NSLog(@"%@", JSON);
    } failure:^(NSError *error, id JSON) {
        //Progress
        [[myProgress sharedInstance] hideIndicator:self];
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error Retrieving"
                                                     message:[NSString stringWithFormat:@"%@",error]
                                                    delegate:nil
                                           cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [av show];
        
    }];
}


- (IBAction)backgrTouch:(id)sender
{
    [self.titlePurchase resignFirstResponder];
}

- (IBAction)btnDSPress:(id)sender
{
    pressedButton = sender;
    datepicker.hidden = NO;
}

- (IBAction)btnDEPress:(id)sender
{
    pressedButton= sender;
    datepicker.hidden = NO;
}

- (IBAction)DateChange:(id)sender
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MMMM-YYYY"];
    [pressedButton setTitle:[dateFormatter stringFromDate:datepicker.date] forState:UIControlStateNormal];
    datepicker.hidden = YES;
}


- (IBAction)addPurchasePress:(id)sender
{
   if (self.titlePurchase.text.length > 0)
   {
       [self addPurchase];
       [self dismissViewControllerAnimated:YES completion:nil];
       
   }
}



- (void)viewDidUnload {
    [self setTitlePurchase:nil];
    [self setDiscriptionPurchase:nil];
    [self setDateStartPurchase:nil];
    [self setDateEndPurchase:nil];
    [self setDatepicker:nil];
    [super viewDidUnload];
}
@end
