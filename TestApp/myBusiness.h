//
//  myBusiness.h
//  TestApp
//
//  Created by alexander_lisovolik on 18.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Models.h"

@interface myBusiness : NSObject
@property (strong, nonatomic) NSString* address;
@property (strong, nonatomic) NSString* city;
@property (strong, nonatomic) NSString* distance;
@property (strong, nonatomic) NSString* idFactual;
@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* phone;
@property (strong, nonatomic) NSString* region;
@property (strong, nonatomic) NSString* zipCode;
+(myBusiness*) getBusinessList: (NSDictionary*) businessDictionary;
@end
