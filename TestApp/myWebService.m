//
//  myWebService.m
//  TestApp
//
//  Created by alexander_lisovolik on 28.07.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "myWebService.h"

static NSString* constMyWebService = @"http://46.162.0.93/localbuys/web/index?r=service/device/";

@implementation myWebService
+ (myWebService*)sharedInstance
{
    static dispatch_once_t pred = 0;
    __strong static myWebService* _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; // or some other init method
    });
    return _sharedObject;
}

- (void) sendRequest:(NSString*) query success:(void (^)(id JSON))success failure:(void (^)(NSError *error, id JSON))failure
{
    NSString *weatherUrl = [NSString stringWithFormat: @"%@%@", constMyWebService, query];
    NSURL *url = [NSURL URLWithString:[weatherUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"%@", url);
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                                    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                                                        if([self validateJSON:JSON])
                                                            success(JSON);
                                                        else
                                                        {
                                                            NSError* error = [NSError errorWithDomain:@"RequestFail" code:10 userInfo:@{@"Error":[JSON objectForKey:@"message"]}];
                                                            failure(error,JSON);
                                                        }
                                                        
                                                    }
                                                    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                                                        failure(error, JSON);
                                                    }

     
     ];
    
    
    [operation start];
     
}

- (BOOL)validateJSON:(NSDictionary*)JSON
{
    return [[JSON objectForKey:@"success"] boolValue];
}
@end
