//
//  MasterViewController.h
//  TestMagRec
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
