//
//  DetailViewController.h
//  TestMagRec
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
