//
//  Note.h
//  TestMagRec
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Note : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSString * keywords;

@end
