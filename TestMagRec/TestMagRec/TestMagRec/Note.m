//
//  Note.m
//  TestMagRec
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "Note.h"


@implementation Note

@dynamic date;
@dynamic title;
@dynamic body;
@dynamic keywords;

@end
