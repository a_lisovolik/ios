//
//  EditNoteControlle.h
//  MagicalNotes
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Notes.h"

@interface EditNoteControlle : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextField *keywordsField;
@property (weak, nonatomic) IBOutlet UITextView *bodyView;
- (id)initWithNote:(Notes *)note;

@end
