//
//  Notes.m
//  MagicalNotes
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "Notes.h"


@implementation Notes

@dynamic date;
@dynamic title;
@dynamic body;
@dynamic keywords;

@end
