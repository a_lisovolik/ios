//
//  AppDelegate.h
//  MagicalNotes
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
