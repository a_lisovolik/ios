//
//  EditNoteControlle.m
//  MagicalNotes
//
//  Created by alexander_lisovolik on 04.08.13.
//  Copyright (c) 2013 alexander_lisovolik. All rights reserved.
//

#import "EditNoteControlle.h"
#import "Notes.h"
#import "ViewController.h"

@interface EditNoteControlle ()
@property (nonatomic, strong) Notes *note;
@property (nonatomic, assign) BOOL isEditing;
@end

@implementation EditNoteControlle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (!self.note)
    {
        self.editing = NO;
    }
    [self setupView];
}

- (void)setupView {
    // Create Cancel Button
    
    if (!self.note) {
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancel:)];
        self.navigationItem.leftBarButtonItem = cancelButton;
    }
    // Create Save Button
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleBordered target:self action:@selector(save:)];
    self.navigationItem.rightBarButtonItem = saveButton;
    if (self.note) {
        // Populate Form Fields
        [self.titleField setText:[self.note title]];
        [self.keywordsField setText:[self.note keywords]];
        [self.bodyView setText:[self.note body]];
    }
}

- (void)cancel:(id)sender {
    // Dismiss View Controller
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)save:(id)sender {
    if (!self.note) {
        // Create Note
        self.note = [Notes MR_createEntity];
        // Configure Note
        [self.note setDate:[NSDate date]];
    }
    // Configure Note
    [self.note setTitle:[self.titleField text]];
    [self.note setKeywords:[self.keywordsField text]];
    [self.note setBody:[self.bodyView text]];
    // Save Managed Object Context
    [[NSManagedObjectContext MR_defaultContext] MR_saveNestedContexts];
    if (self.isEditing) {
        // Pop View Controller from Navigation Stack
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        // Dismiss View Controller
        ViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [vc fetchNotes];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (id)initWithNote:(Notes *)note {
    self = [self initWithNibName:@"EditNoteControlle" bundle:[NSBundle mainBundle]];
    if (self) {
        // Set Note
        self.note = note;
        // Set Flag
        self.isEditing = YES;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
